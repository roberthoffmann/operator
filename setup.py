#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from setuptools import setup, find_namespace_packages

setup(
    name="yaook-operators",
    version="0.0.1",
    packages=find_namespace_packages(include=["yaook.*"]),
    install_requires=[
        "kubernetes-asyncio==24.2.3",
        "jsonpatch==1.33",
        "Jinja2==3.1.2",
        "cryptography==41.0.4",
        "environ-config==23.2.0",
        "nose==1.3.7",
        "ddt==1.6.0",
        "openstacksdk==1.5.0",
        "oslo.config==9.2.0",
        "oslo.policy==4.2.1",
        "pyOpenSSL==23.2.0",
        "python-dxf==11.1.1",
        "python-ironicclient==5.4.0",
        "semver==3.0.1",
        "pymysql==1.1.0",
        "pyyaml==6.0.1",
        "python-novaclient==18.4.0",
        "opentelemetry-api==1.20.0",
        "opentelemetry-sdk==1.20.0",
        "opentelemetry-exporter-jaeger==1.20.0",
        "lru-dict==1.2.0",
    ],
    include_package_data=True,
)
