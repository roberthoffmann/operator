Operators
=========

Operators are the primary tool Yaook uses for the resource management in Kubernetes.

Operator overview
-----------------

Operators generally take some kind of Kubernetes native resource request (mostly using custom resource definitions).
An Operator is then running a reconcile loop to implement the resource request.

If you want to know more about Operators in general please take a look here: https://kubernetes.io/docs/concepts/extend-kubernetes/operator/

As the deployment of OpenStack services needs to follow specific steps most of the Yaook Operators are leveraging a state machine.
This approach conflicts with the Cloud concept of eventual consistency where
you would just spawn all resources at once and let them take care of the synchronization.
But it allows us to define dependencies for certain resources, e.g. running database migrations for an upgrade, before upgrading the services.

If it becomes necessary to stop an operator from doing anything, one can annotate its customresource with the :data:`~.ANNOTATION_PAUSE` annotation. The presence of this annotation prevents the operator from starting another reconciliation run.

Operator implementation
-----------------------

Operators watch the Kubernetes API for changes in their custom resources and execute the appropriate steps to implement them afterwards.
The Operators might also take other, externally managed, Kubernetes resources into account when implementing or updating the deployed resources.

.. image:: figures/operator_implementation.drawio.svg

To ensure the resources do not drift from the desired state the operators regularly reconcile the resources even if no changes occure.
