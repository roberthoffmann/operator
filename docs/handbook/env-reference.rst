.. _uref-operator-env-vars:

Environment Variable Reference
==============================

.. envvar:: YAOOK_OP_NAMESPACE

    The Kubernetes Namespace the Operator watches for custom resources.

    Affects: all operators.

.. envvar:: YAOOK_OP_VERBOSITY

    The Log-Level of the Operator. Can be set to 0 (ERROR), 1 (WARNING), 2 (INFO), 3 (DEBUG)

    Affects: all operators.

.. envvar:: YAOOK_OP_DOCKER_CONFIG

    The Path to a Docker config file containg the credentials for an authenticated Docker registry. Only required when automatically updating (or during development) and using some registry besides the official Gitlab.

    Affects: all operators.

.. envvar:: YAOOK_OP_DOCKER_REGISTRY_BASE_PATH

    Allows overriding the base image path for the Yaook images.

    Default: ``registry.yaook.cloud/yaook``

    Affects: all operators.

.. envvar:: YAOOK_OP_DOCKER_REGISTRY_OVERRIDE

    Allows overriding the host names of container image registries there the container images are pulled from. If set, this must be a valid yaml dictionary.

    The key must be the registry to be overwritten. The special key ``DEFAULT_REGISTRY`` overrides the default registry which matches all image paths which are not prefixed with a certain registry.

    The value must be the new registry host name.

    For example to replace the registry for all images pulled from the default registry, e.g. the Bitnami images, and images from registry.yaook.cloud, :envvar:`YAOOK_OP_DOCKER_REGISTRY_OVERRIDE` must be set to:

    .. code-block:: json

        {"DEFAULT_REGISTRY": "my-registry.domain.tld", "registry.yaook.cloud": "my-registry.domain.tld"}

    Note that if used in conjunction with :envvar:`YAOOK_OP_VERSIONS_OVERRIDE` the registry name is overwritten first. Can cause problems if used together with :envvar:`YAOOK_OP_DOCKER_REGISTRY_BASE_PATH` if the Yaook image registry has been overwritten.

    Affects: all operators.

.. envvar:: YAOOK_OP_DOCKER_INSECURE

    Allows disabling HTTPS when talking to docker registries. Don't set this.

    Affects: all operators.

.. envvar:: YAOOK_OP_DOCKER_TLS_SKIP_VERIFY

    Allows disabling TLS verification when talking to docker registries. Don't set this.

    Affects: all operators.

.. envvar:: YAOOK_OP_CLUSTER_DOMAIN

    The cluster domain of the Kubernetes cluster the Operator is connecting to. If the Operator is running inside the cluster this is automatically discovered and does not need to be set.

    Affects: all operators.

.. envvar:: YAOOK_OP_VERSIONS_USE_ALPHA

    If True allow the Operator to use Docker images that are flagged as alpha. This is only used for images which are not currently pinned. Nowadays, this is generally only the case while re-generating the pin file.

    Affects: all operators.

.. envvar:: YAOOK_OP_VERSIONS_USE_ROLLING

    If True allow the Operator to use Docker images that are flagged as rolling. This is only used for images which are not currently pinned. Nowadays, this is generally only the case while re-generating the pin file.

    Affects: all operators.

.. envvar:: YAOOK_OP_VERSIONS_OVERRIDE

    Allows overriding the url of a external dependency (e.g. a Docker image). Must be set to a valid yaml dictionary. The key must be the full original url as in `pinned_version.yml` while the value must be the full URL (including version) of the dependency to use. Note that if used in conjunction with :envvar:`YAOOK_OP_DOCKER_REGISTRY_OVERRIDE` the registry name will be overwritten first so the key for the version overwrite must match the new registry name to apply.

    Affects: all operators.

.. envvar:: YAOOK_OP_RUNNER_COUNT

    The amount of task queue runners that reconcile the different custom resources

    Default: ``3``

    Affects: all operators.

.. envvar:: YAOOK_OP_PERIODIC_RECONCILE_DELAY

    The amount of time in seconds after that each resource is reconciled again.

    Default: ``3600``

    Affects: all operators.

.. envvar:: YAOOK_OP_TRACING_ENABLED

    Enables the support for sending traces to jaeger via opentelemetry.

    Affects: all operators.

.. envvar:: YAOOK_OP_JEAGER_HOSTNAME

    Defines the Hostname of the Jaeger Endpoint.

    Default: ``localhost``

    Affects: all operators.

.. envvar:: YAOOK_OP_JEAGER_PORT

    Defines the Port of the Jaeger Endpoint.

    Default: ``6831``

    Affects: all operators.

.. envvar:: YAOOK_KEYSTONE_OP_AUTH_URL

    .. deprecated:: (something ancient)

        This environment variable has no effect anymore and can safely be removed.

    Affects: keystone-resources-operator.

.. envvar:: YAOOK_KEYSTONE_OP_INTERFACE

    The OpenStack interface the Keystone Operator should use to connect to Keystone.

    Default: ``internal``

    Affects: keystone-resources-operator.

.. envvar:: YAOOK_NEUTRON_DHCP_AGENT_OP_INTERFACE

    The OpenStack interface the Neutron DHCP Operator should use to connect to Keystone.

    Default: ``internal``

    Affects: neutron-dhcp-operator.

.. envvar:: YAOOK_NEUTRON_L2_AGENT_OP_INTERFACE

    The OpenStack interface the Neutron L2 Operator should use to connect to Keystone.

    Default: ``internal``

    Affects: neutron-l2-operator.

.. envvar:: YAOOK_NEUTRON_DHCP_L3_OP_INTERFACE

    The OpenStack interface the Neutron L3 Operator should use to connect to Keystone.

    Default: ``internal``

    Affects: neutron-l3-operator.

.. envvar:: YAOOK_NEUTRON_L3_AGENT_OP_JOB_IMAGE

    A reference to the Docker image that should be used to evacuate l3 agents.

    Affects: neutron-l3-operator.

.. envvar:: YAOOK_NEUTRON_BGP_DRAGENT_OP_INTERFACE

    The OpenStack interface the Neutron BGP Dynamic Routing Agent Operator should use to connect to Keystone.

    Affects: neutron-bgpdragent-operator.

.. envvar:: YAOOK_NEUTRON_BGP_DRAGENT_OP_JOB_IMAGE

    A reference to the Docker image that should be used to evacuate bgp agents.

    Affects: neutron-bgpdragent-operator.

.. envvar:: YAOOK_NOVA_COMPUTE_OP_INTERFACE

    The OpenStack interface the Nova Compute Operator should use to connect to Keystone.

    Affects: nova-compute-operator.

.. envvar:: YAOOK_NOVA_COMPUTE_OP_JOB_IMAGE

    A reference to the Docker image that should be used to evacuate compute nodes.

    Affects: nova-compute-operator.

.. envvar:: YAOOK_INFRA_OP_AMQP_INTER_NODE_TLS_VERIFICATION

    Enable inter-node TLS verification for RabbitMQ servers.

    Default: ``true``.

    Affects: infra-operator.

.. envvar:: REQUESTS_CA_BUNDLE

    Path to a file to override the CA certificates trusted by HTTPS client operations done by the operators.
