import os
import unittest
import unittest.mock

from yaook.op.nova_compute.eviction_manager import EvictionManager


class TestEvictionManager(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()
        self._env_backup = os.environ.pop("EVICT_MANAGER_NAMESPACE", None)
        self._env_backup = os.environ.pop("EVICT_MANAGER_INTERVAL", None)
        self._env_backup = os.environ.pop(
            "EVICT_MANAGER_MAX_PER_HOUR", None
        )
        self._env_backup = os.environ.pop(
            "YAOOK_NOVA_COMPUTE_OP_JOB_IMAGE", None
        )

        os.environ["EVICT_MANAGER_NAMESPACE"] = "yaook"
        os.environ["EVICT_MANAGER_INTERVAL"] = "10"
        os.environ["EVICT_MANAGER_MAX_PER_HOUR"] = "1"
        os.environ["YAOOK_NOVA_COMPUTE_OP_JOB_IMAGE"] = "test_image"

    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager._connect"
    )
    def test_get_newly_down_computenodes_new_down(self, mock_connect):

        mock_hypervisor1 = unittest.mock.MagicMock()
        mock_hypervisor1.name = "hypervisor1"
        mock_hypervisor2 = unittest.mock.MagicMock()
        mock_hypervisor2.name = "hypervisor2"
        mock_hypervisor3 = unittest.mock.MagicMock()
        mock_hypervisor3.name = "hypervisor3"

        mock_client = unittest.mock.MagicMock()
        mock_client.compute = unittest.mock.MagicMock()
        mock_client.compute.hypervisors.return_value = [
            mock_hypervisor1,
        ]
        mock_connect.return_value = mock_client
        eviction_manager = EvictionManager(
            connection_info=None, k8s_client=None
        )
        mock_client.compute.hypervisors.return_value = [
            mock_hypervisor2,
            mock_hypervisor3,
        ]
        result = eviction_manager.get_newly_down_computenodes()
        self.assertEqual(len(result), 2)
        self.assertTrue("hypervisor2" in result)
        self.assertTrue("hypervisor3" in result)

    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager._connect"
    )
    def test_get_newly_down_computenodes_already_down(self, mock_connect):

        mock_hypervisor1 = unittest.mock.MagicMock()
        mock_hypervisor1.name = "hypervisor1"
        mock_hypervisor2 = unittest.mock.MagicMock()
        mock_hypervisor2.name = "hypervisor2"

        mock_client = unittest.mock.MagicMock()
        mock_client.compute = unittest.mock.MagicMock()
        mock_client.compute.hypervisors.return_value = [
            mock_hypervisor1,
            mock_hypervisor2,
        ]
        mock_connect.return_value = mock_client
        eviction_manager = EvictionManager(
            connection_info=None, k8s_client=None
        )
        eviction_manager.previous_down = {"hypervisor1"}
        result = eviction_manager.get_newly_down_computenodes()
        self.assertEqual(len(result), 1)
        self.assertTrue("hypervisor1" not in result)
        self.assertTrue("hypervisor2" in result)

    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager.interfaces."
        "nova_compute_node_interface"
    )
    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager._connect"
    )
    async def test_checked_managed_by_this_yaook(
        self, mock_connect, mock_interface
    ):

        interface = unittest.mock.MagicMock()
        interface.list_ = unittest.mock.AsyncMock()
        interface.list_.return_value = [{"status": {"state": "Enabled"}}]
        mock_interface.return_value = interface
        mock_connect.return_value = None
        eviction_manager = EvictionManager(
            connection_info={}, k8s_client=unittest.mock.MagicMock()
        )
        check_value = await eviction_manager.check_if_managed_by_yaook(
            "hypervisor1"
        )
        self.assertTrue(check_value)

    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager.interfaces."
        "nova_compute_node_interface"
    )
    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager._connect"
    )
    async def test_checked_managed_by_this_yaook_not_Enabled(
        self, mock_connect, mock_interface
    ):

        interface = unittest.mock.MagicMock()
        interface.list_ = unittest.mock.AsyncMock()
        interface.list_.return_value = [{"status": {"state": "Creating"}}]
        mock_interface.return_value = interface
        mock_connect.return_value = None
        eviction_manager = EvictionManager(
            connection_info={}, k8s_client=unittest.mock.MagicMock()
        )
        check_value = await eviction_manager.check_if_managed_by_yaook(
            "hypervisor1"
        )
        self.assertFalse(check_value)

    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager.interfaces."
        "nova_compute_node_interface"
    )
    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager._connect"
    )
    async def test_not_checked_managed_by_this_nova(
        self, mock_connect, mock_interface
    ):

        interface = unittest.mock.MagicMock()
        interface.list_ = unittest.mock.AsyncMock()
        interface.list_.return_value = []
        mock_interface.return_value = interface
        mock_connect.return_value = None
        eviction_manager = EvictionManager(
            connection_info={}, k8s_client=unittest.mock.MagicMock()
        )
        check_value = await eviction_manager.check_if_managed_by_yaook(
            "hypervisor1"
        )
        self.assertFalse(check_value)

    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager.interfaces."
        "job_interface"
    )
    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager._connect"
    )
    async def test_check_if_evict_job_exists(
        self, mock_connect, mock_interface
    ):

        interface = unittest.mock.MagicMock()
        interface.list_ = unittest.mock.AsyncMock()
        interface.list_.return_value = ["lalalulu"]
        mock_interface.return_value = interface
        mock_connect.return_value = None
        eviction_manager = EvictionManager(
            connection_info={}, k8s_client=unittest.mock.MagicMock()
        )
        check_value = await eviction_manager.check_if_evict_job_exists(
            "hypervisor1"
        )
        self.assertTrue(check_value)

    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager.interfaces."
        "job_interface"
    )
    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager._connect"
    )
    async def test_check_if_evict_job_not_exists(
        self, mock_connect, mock_interface
    ):

        interface = unittest.mock.MagicMock()
        interface.list_ = unittest.mock.AsyncMock()
        interface.list_.return_value = []
        mock_interface.return_value = interface
        mock_connect.return_value = None
        eviction_manager = EvictionManager(
            connection_info={}, k8s_client=unittest.mock.MagicMock()
        )
        check_value = await eviction_manager.check_if_evict_job_exists(
            "hypervisor1"
        )
        self.assertFalse(check_value)

    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager._connect"
    )
    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager.PatchingApiClient"
    )
    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager.NovaComputeNode."
        "get_resource_interface"
    )
    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager.NovaComputeNode."
        "api_state._background_job.reconcile"
    )
    async def test_spawn_eviction_job(
        self,
        mock_reconcile,
        mock_get_resource_interface,
        mock_patching_api_client,
        mock_connect,
    ):

        mock_connect.return_value = None
        mock_interface = unittest.mock.MagicMock()
        mock_interface.read = unittest.mock.AsyncMock()
        mock_interface.read.return_value = {"status": {"eviction": {}}}

        mock_interface.delete = unittest.mock.AsyncMock()
        mock_interface.delete.side_effect = None

        mock_get_resource_interface.return_value = mock_interface

        mock_reconcile.return_value = unittest.mock.AsyncMock

        eviction_manager = EvictionManager(
            connection_info={}, k8s_client=unittest.mock.MagicMock()
        )

        await eviction_manager.spawn_eviction_job("dummy_node")

        mock_interface.delete.assert_awaited_once_with(
            namespace="yaook", name="dummy_node"
        )
        mock_reconcile.assert_awaited_once()

    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager._connect"
    )
    async def test_iterate_no_node_down(self, mock_connect):

        mock_connect.return_value = None
        eviction_manager = EvictionManager(
            connection_info={}, k8s_client=unittest.mock.MagicMock()
        )
        eviction_manager.get_newly_down_computenodes = \
            unittest.mock.MagicMock()
        eviction_manager.process_node = unittest.mock.AsyncMock()
        eviction_manager.get_newly_down_computenodes.side_effect = [[]]
        eviction_manager.process_node.return_value = None
        await eviction_manager.eviction_manager_iteration_loop()
        eviction_manager.process_node.assert_not_called()

    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager._connect"
    )
    async def test_iterate_one_node_down(self, mock_connect):

        mock_connect.return_value = None
        eviction_manager = EvictionManager(
            connection_info={}, k8s_client=unittest.mock.MagicMock()
        )
        eviction_manager.get_newly_down_computenodes = \
            unittest.mock.MagicMock()
        eviction_manager.process_node = unittest.mock.AsyncMock()
        eviction_manager.get_newly_down_computenodes.side_effect = [
            ["hypervisor1"]
        ]
        eviction_manager.process_node.return_value = None
        eviction_manager.check_if_managed_by_yaook = unittest.mock.AsyncMock()
        eviction_manager.check_if_managed_by_yaook.return_value = True
        await eviction_manager.eviction_manager_iteration_loop()
        eviction_manager.process_node.assert_called_once_with("hypervisor1")

    @unittest.mock.patch(
        "yaook.op.nova_compute.eviction_manager._connect"
    )
    async def test_iterate_too_many_nodes_down(self, mock_connect):

        mock_connect.return_value = None
        eviction_manager = EvictionManager(
            connection_info={}, k8s_client=unittest.mock.MagicMock()
        )
        eviction_manager.get_newly_down_computenodes = \
            unittest.mock.MagicMock()
        eviction_manager.process_node = unittest.mock.AsyncMock()
        eviction_manager.get_newly_down_computenodes.side_effect = [
            ["hypervisor1", "hypervisor2"]
        ]
        eviction_manager.process_node.return_value = None
        eviction_manager.check_if_managed_by_yaook = unittest.mock.AsyncMock()
        eviction_manager.check_if_managed_by_yaook.return_value = True
        await eviction_manager.eviction_manager_iteration_loop()
        eviction_manager.process_node.assert_not_called()
