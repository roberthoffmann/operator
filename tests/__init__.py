#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest


class TestCase(unittest.TestCase):
    def _assert_subdict_equals(self, real, expected):
        def intersect(real, expected):
            if isinstance(expected, dict):
                out = {}
                for k in expected.keys():
                    if k in real:
                        if isinstance(expected[k], dict) or \
                                isinstance(expected[k], list):
                            out[k] = intersect(real[k], expected[k])
                        else:
                            out[k] = real[k]
            else:
                out = []
                for i in range(len(expected)):
                    if i < len(real):
                        if isinstance(expected[i], dict) or \
                                isinstance(expected[i], list):
                            out.append(intersect(real[i], expected[i]))
                        else:
                            out.append(real[i])
            return out
        real_limit = intersect(real, expected)
        self.assertEqual(real_limit, expected)
