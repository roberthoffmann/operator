import contextlib
import copy
import unittest

import kubernetes_asyncio.client as kclient

import yaook.statemachine.context as context
import yaook.statemachine.exceptions as exceptions
import yaook.statemachine.templating as templating

import yaook.statemachine.resources.yaook_cds as yaook_cds

from .utils import (
    ResourceTestMixin,
    ResourceInterfaceMock,
    TemplateTestMixin,
    wrap_node_list,
)


class TestConfiguredDaemonSet(unittest.IsolatedAsyncioTestCase):
    class ConfiguredDaemonSetTest(ResourceTestMixin,
                                  yaook_cds.ConfiguredDaemonSet):
        pass

    def setUp(self):
        self.ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.cds = self.ConfiguredDaemonSetTest(
            scheduling_keys=["foo", "bar"],
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.deep_has_changes")
    def test_needs_update_uses_deep_has_changes(
            self,
            deep_has_changes):
        v1 = unittest.mock.MagicMock()
        v2 = unittest.mock.MagicMock()
        deep_has_changes.return_value = unittest.mock.sentinel.has_changes
        result = self.cds._needs_update(v1, v2)

        self.assertEqual(result, unittest.mock.sentinel.has_changes)

    def test_needs_update_ignores_updated_at_timestamp(self):
        v1 = {
            "metadata": {
                "labels": {
                    "foo": "v1",
                },
                "annotations": {
                    "foo": "v1",
                    context.ANNOTATION_LAST_UPDATE: "v1",
                }
            },
            "spec": unittest.mock.sentinel.specv1,
        }

        v2 = {
            "metadata": {
                "labels": {
                    "foo": "v1",
                },
                "annotations": {
                    "foo": "v1",
                    context.ANNOTATION_LAST_UPDATE: "v2",
                }
            },
            "spec": unittest.mock.sentinel.specv1,
        }

        self.assertFalse(self.cds._needs_update(v1, v2))

    def test_needs_update_considers_other_annotations(self):
        v1 = {
            "metadata": {
                "labels": {
                    "foo": "v1",
                },
                "annotations": {
                    "foo": "v1",
                    context.ANNOTATION_LAST_UPDATE: "v1",
                }
            },
            "spec": unittest.mock.sentinel.specv1,
        }

        v2 = {
            "metadata": {
                "labels": {
                    "foo": "v1",
                },
                "annotations": {
                    "foo": "v2",
                    context.ANNOTATION_LAST_UPDATE: "v1",
                }
            },
            "spec": unittest.mock.sentinel.specv1,
        }

        self.assertTrue(self.cds._needs_update(v1, v2))

    def test_needs_update_considers_labels(self):
        v1 = {
            "metadata": {
                "labels": {
                    "foo": "v1",
                },
                "annotations": {
                    "foo": "v1",
                    context.ANNOTATION_LAST_UPDATE: "v1",
                }
            },
            "spec": unittest.mock.sentinel.specv1,
        }

        v2 = {
            "metadata": {
                "labels": {
                    "foo": "v2",
                },
                "annotations": {
                    "foo": "v1",
                    context.ANNOTATION_LAST_UPDATE: "v1",
                }
            },
            "spec": unittest.mock.sentinel.specv1,
        }

        self.assertTrue(self.cds._needs_update(v1, v2))

    def test_needs_update_considers_spec(self):
        v1 = {
            "metadata": {
                "labels": {
                    "foo": "v1",
                },
                "annotations": {
                    "foo": "v1",
                    context.ANNOTATION_LAST_UPDATE: "v1",
                }
            },
            "spec": unittest.mock.sentinel.specv1,
        }

        v2 = {
            "metadata": {
                "labels": {
                    "foo": "v1",
                },
                "annotations": {
                    "foo": "v1",
                    context.ANNOTATION_LAST_UPDATE: "v1",
                }
            },
            "spec": unittest.mock.sentinel.specv2,
        }

        self.assertTrue(self.cds._needs_update(v1, v2))

    def test_does_not_modify_new_version(self):
        v1 = {
            "metadata": {
                "labels": {
                    "foo": "v1",
                },
                "annotations": {
                    "foo": "v1",
                    context.ANNOTATION_LAST_UPDATE: "v1",
                }
            },
            "spec": unittest.mock.sentinel.specv1,
        }

        v2 = {
            "metadata": {
                "labels": {
                    "foo": "v1",
                },
                "annotations": {
                    "foo": "v1",
                    context.ANNOTATION_LAST_UPDATE: "v1",
                }
            },
            "spec": unittest.mock.sentinel.specv2,
        }
        v2_bak = copy.deepcopy(v2)

        self.assertTrue(self.cds._needs_update(v1, v2))

        self.assertEqual(v2, v2_bak)

    async def test_get_relevant_nodes(self):
        ctx = unittest.mock.Mock(["api_client"])

        def node_mock(name):
            mock = unittest.mock.Mock([])
            mock.metadata = unittest.mock.Mock([])
            mock.metadata.name = name
            return mock
        testnodes = [
            node_mock("node1"),
            node_mock("node2"),
            node_mock("node3"),
        ]

        with contextlib.ExitStack() as stack:
            get_selected_nodes_union = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.get_selected_nodes_union",
            ))
            get_selected_nodes_union.return_value = testnodes

            _scheduling_keys_to_selectors = stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.resources.yaook_cds."
                    "_scheduling_keys_to_selectors",
                ),
            )

            relevant_nodes = await self.cds._get_relevant_nodes(ctx)

        _scheduling_keys_to_selectors.assert_called_once_with(
            ["foo", "bar"],
        )
        get_selected_nodes_union.assert_called_once_with(
            ctx.api_client,
            _scheduling_keys_to_selectors(),
        )

        self.assertCountEqual(
            relevant_nodes,
            testnodes,
        )

    async def test_adopt_object_injects_target_nodes_by_scheduling_keys(self):
        ctx = unittest.mock.Mock(["api_client"])

        def node_mock(name):
            mock = unittest.mock.Mock([])
            mock.metadata = unittest.mock.Mock([])
            mock.metadata.name = name
            return mock

        object_ = {}
        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object",
            ))

            get_selected_nodes_union = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.get_selected_nodes_union",
            ))
            get_selected_nodes_union.return_value = [
                node_mock("node1"),
                node_mock("node2"),
                node_mock("node3"),
            ]

            _scheduling_keys_to_selectors = stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.resources.yaook_cds."
                    "_scheduling_keys_to_selectors",
                ),
            )

            await self.cds.adopt_object(ctx, object_)

        adopt_object.assert_called_once_with(ctx, object_)

        _scheduling_keys_to_selectors.assert_called_once_with(
            ["foo", "bar"],
        )
        get_selected_nodes_union.assert_called_once_with(
            ctx.api_client,
            _scheduling_keys_to_selectors(),
        )

        self.assertCountEqual(
            object_["spec"]["targetNodes"],
            ["node1", "node2", "node3"],
        )

    async def test_adopt_object_injects_tolerations_by_scheduling_keys(self):
        ctx = unittest.mock.Mock(["api_client"])
        object_ = {}
        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object",
            ))

            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value.list_node = unittest.mock.AsyncMock()
            CoreV1Api.return_value.list_node.return_value = wrap_node_list([])

            await self.cds.adopt_object(ctx, object_)

        adopt_object.assert_called_once_with(ctx, object_)

        self.assertCountEqual(
            object_["spec"]["template"]["spec"]["tolerations"],
            [
                {
                    "key": "foo",
                    "operator": "Exists",
                },
                {
                    "key": "bar",
                    "operator": "Exists",
                },
            ]
        )

    async def test_adopt_object_extends_existing_tolerations(self):
        ctx = unittest.mock.Mock(["api_client"])
        object_ = {
            "spec": {
                "template": {
                    "spec": {
                        "tolerations": [
                            unittest.mock.sentinel.dummy,
                        ]
                    }
                }
            }
        }
        with contextlib.ExitStack() as stack:
            adopt_object = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.resources.SingleObject.adopt_object",
            ))

            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value.list_node = unittest.mock.AsyncMock()
            CoreV1Api.return_value.list_node.return_value = wrap_node_list([])

            await self.cds.adopt_object(ctx, object_)

        adopt_object.assert_called_once_with(ctx, object_)

        self.assertCountEqual(
            object_["spec"]["template"]["spec"]["tolerations"],
            [
                unittest.mock.sentinel.dummy,
                {
                    "key": "foo",
                    "operator": "Exists",
                },
                {
                    "key": "bar",
                    "operator": "Exists",
                },
            ]
        )

    async def test_get_used_resources_returns_empty_for_absent_cds(self):
        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.cds, "_get_current",
            ))
            _get_current.side_effect = exceptions.ResourceNotPresent(
                unittest.mock.sentinel.foo,
                unittest.mock.Mock(),
            )

            result = await self.cds.get_used_resources(self.ctx)

        _get_current.assert_called_once_with(self.ctx, True)

        self.assertCountEqual(result, [])

    async def test_get_used_resources_extracts_resources_from_cds_spec(self):  # NOQA
        pod_spec = unittest.mock.sentinel.pod_spec

        cds = {
            "spec": {
                "template": {
                    "spec": pod_spec,
                },
            },
            "metadata": {
                "uid": "42"
            },
        }

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.cds, "_get_current",
            ))
            _get_current.return_value = cds

            extract_cds_references = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_cds_references",
            ))
            extract_cds_references.return_value = [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ]

            extract_pod_references = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_pod_references",
            ))
            extract_pod_references.return_value = []

            pod_interface_mock = ResourceInterfaceMock()
            pod_interface = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.pod_interface",
            ))
            pod_interface.return_value = pod_interface_mock
            pod_interface_mock.list_.return_value = []

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.LabelSelector.from_dict",
            ))

            result = await self.cds.get_used_resources(self.ctx)

        extract_cds_references.assert_called_once_with(
            cds["spec"],
            self.ctx.namespace,
        )

        self.assertCountEqual(
            result,
            [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ],
        )

    async def test_get_used_resources_extracts_resources_from_existing_pods(self):  # NOQA
        pod_spec = unittest.mock.sentinel.pod_spec

        cds_uid = "42"
        cds = {
            "spec": {
                "template": {
                    "spec": pod_spec,
                },
            },
            "metadata": {
                "uid": cds_uid
            },
        }

        def generate_resources():
            yield [
                unittest.mock.sentinel.r1p1,
                unittest.mock.sentinel.r2p1,
            ]
            yield [
                unittest.mock.sentinel.r1p2,
                unittest.mock.sentinel.r2p2,
            ]
            yield [
                unittest.mock.sentinel.r1p3,
                unittest.mock.sentinel.r2p3,
            ]

        def mock_pod(spec):
            return kclient.V1Pod(spec=spec)

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.cds, "_get_current",
            ))
            _get_current.return_value = cds

            extract_cds_references = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_cds_references",
            ))
            extract_cds_references.return_value = [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
            ]

            extract_pod_references = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_pod_references",
            ))
            extract_pod_references.side_effect = generate_resources()

            pod_interface_mock = ResourceInterfaceMock()
            pod_interface = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.interfaces.pod_interface",
            ))
            pod_interface.return_value = pod_interface_mock
            pod_interface_mock.list_.return_value = [
                mock_pod(unittest.mock.sentinel.pod1),
                mock_pod(unittest.mock.sentinel.pod2),
                mock_pod(unittest.mock.sentinel.pod3),
            ]

            result = await self.cds.get_used_resources(self.ctx)

        pod_interface.assert_called_once_with(
            self.ctx.api_client,
        )

        pod_interface_mock.list_.assert_awaited_once_with(
            self.ctx.namespace,
            label_selector=f"{context.LABEL_CDS_CONTROLLER_UID}={cds_uid}"
        )

        extract_cds_references.assert_called_once_with(
            cds["spec"],
            self.ctx.namespace,
        )

        self.assertCountEqual(
            extract_pod_references.mock_calls,
            [
                unittest.mock.call(unittest.mock.sentinel.pod1,
                                   self.ctx.namespace),
                unittest.mock.call(unittest.mock.sentinel.pod2,
                                   self.ctx.namespace),
                unittest.mock.call(unittest.mock.sentinel.pod3,
                                   self.ctx.namespace),
            ],
        )

        self.assertCountEqual(
            result,
            [
                unittest.mock.sentinel.r1,
                unittest.mock.sentinel.r2,
                unittest.mock.sentinel.r3,
                unittest.mock.sentinel.r1p1,
                unittest.mock.sentinel.r2p1,
                unittest.mock.sentinel.r1p2,
                unittest.mock.sentinel.r2p2,
                unittest.mock.sentinel.r1p3,
                unittest.mock.sentinel.r2p3,
            ],
        )


class TestTemplatedConfiguredDaemonSet(TemplateTestMixin, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.value = {
           "key1": "value with \"complex\" escaping 'requirements'",
           "key2": 23,
        }
        self.tm = yaook_cds.TemplatedConfiguredDaemonSet(
            template=None,
            templatedir=str(self._template_path),
            scheduling_keys=["foo", "bar"],
        )

    def test__add_filters_registers_volume_template_filters(self):
        fake_env = unittest.mock.Mock([])
        fake_env.filters = {}

        self.tm._add_filters(fake_env)

        self.assertEqual(fake_env.filters["to_config_map_volumes"],
                         templating.to_config_map_volumes)
        self.assertEqual(fake_env.filters["to_secret_volumes"],
                         templating.to_secret_volumes)
