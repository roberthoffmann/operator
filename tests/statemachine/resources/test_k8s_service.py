import unittest
from unittest.mock import sentinel

import kubernetes_asyncio.client as kclient

import yaook.statemachine.resources.k8s_service as k8s_service

from .utils import ResourceTestMixin


class TestService(unittest.TestCase):
    class ServiceTest(ResourceTestMixin, k8s_service.Service):
        pass

    def test_needs_update_returns_true_if_port_added(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                ports=[],
            )
        )

        new = {
            "spec": {
                "ports": [
                    {
                        "name": "foo",
                        "port": 31416,
                        "protocol": "TCP",
                        "targetPort": 31416,
                    }
                ]
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_returns_true_if_port_modified(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                ports=[
                    kclient.V1ServicePort(
                        name="foo",
                        port=31415,
                        protocol="TCP",
                        target_port=31416,
                    ),
                ],
            )
        )

        new = {
            "spec": {
                "ports": [
                    {
                        "name": "foo",
                        "port": 31416,
                        "protocol": "TCP",
                        "targetPort": 31416,
                    }
                ]
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_returns_true_if_port_removed(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                ports=[
                    kclient.V1ServicePort(
                        name="foo",
                        port=31416,
                        protocol="TCP",
                        target_port=31416,
                    ),
                ],
            )
        )

        new = {
            "spec": {
                "ports": []
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_returns_true_if_selector_changed(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                selector=sentinel.old_selector,
                ports=[],
            )
        )

        new = {
            "spec": {
                "selector": sentinel.new_selector,
                "ports": [],
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_missing_selector(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                selector=sentinel.old_selector,
                ports=[],
            )
        )

        new = {
            "spec": {
                "ports": [],
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_missing_port_and_selector(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
            )
        )

        new = {
            "spec": {
            }
        }

        self.assertFalse(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_missing_default_type(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                type="ClusterIP"
            )
        )

        new = {
            "spec": {
            }
        }

        self.assertFalse(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_different_type_unset(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                type="LoadBalancer"
            )
        )

        new = {
            "spec": {
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_different_type(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                type="LoadBalancer"
            )
        )

        new = {
            "spec": {
                "type": "ClusterIP",
            }
        }

        self.assertTrue(
            s._needs_update(old, new)
        )

    def test_needs_update_handles_same_type(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                type="LoadBalancer"
            )
        )

        new = {
            "spec": {
                "type": "LoadBalancer",
            }
        }

        self.assertFalse(
            s._needs_update(old, new)
        )

    def test_needs_update_returns_false_if_equal(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                selector=sentinel.common_selector,
                ports=[
                    kclient.V1ServicePort(
                        name="foo",
                        port=31416,
                        protocol="TCP",
                        target_port=31416,
                    ),
                ],
            )
        )

        new = {
            "spec": {
                "selector": sentinel.common_selector,
                "ports": [
                    {
                        "name": "foo",
                        "port": 31416,
                        "protocol": "TCP",
                        "targetPort": 31416,
                    }
                ],
            }
        }

        self.assertFalse(
            s._needs_update(old, new)
        )

    def test_needs_update_returns_true_on_label_change(self):
        s = self.ServiceTest()

        old = kclient.V1Service(
            spec=kclient.V1ServiceSpec(
                ports=[],
            ),
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )

        new = {
            "spec": {},
            "metadata": {
                "labels": {
                    "foo": "foo",
                    "bar": "bar",
                },
            },
        }

        self.assertTrue(
            s._needs_update(old, new)
        )
