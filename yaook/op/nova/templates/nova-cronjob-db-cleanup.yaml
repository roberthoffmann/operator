##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: batch/v1
kind: CronJob
metadata:
  generateName: "nova-db-cleanup-"
spec:
  schedule: {{ crd_spec.databaseCleanup.schedule }}
  concurrencyPolicy: Forbid
  jobTemplate:
    spec:
      template:
        spec:
          automountServiceAccountToken: false
          enableServiceLinks: false
          containers:
            - name: nova-db-cleanup
              image:  {{ versioned_dependencies['nova_docker_image'] }}
              command: ["/db_cleanup.sh"]
              volumeMounts:
                - name: nova-config-volume
                  mountPath: "/etc/nova"
                - name: ca-certs
                  mountPath: /etc/ssl/certs
              env:
                - name: REQUESTS_CA_BUNDLE
                  value: /etc/ssl/certs/ca-bundle.crt
                - name: DELETION_TIME_RANGE
                  value: {{ crd_spec.databaseCleanup.deletionTimeRange | string }}
          {% if crd_spec.targetRelease == 'queens' %}
                - name: DB_USER
                  value: {{ params.db_username }}
                - name: DB_HOST
                  value: {{ dependencies['cell1_db_service'].resource_name() }}
                - name: DB_PASSWORD
                  valueFrom:
                    secretKeyRef:
                      name: {{ dependencies['cell1_db_api_user_password'].resource_name() }}
                      key: password
          {% endif %}
              resources: {{ crd_spec | resources('job.nova-db-cleanup-cronjob') }}
          volumes:
            - name: nova-config-volume
              secret:
                secretName: {{ dependencies['config'].resource_name() }}
                items:
                  - key: nova.conf
                    path: nova.conf
            - name: ca-certs
              configMap:
                name: {{ dependencies['ca_certs'].resource_name() }}
    {% if crd_spec.imagePullSecrets | default(False) %}
          imagePullSecrets: {{ crd_spec.imagePullSecrets }}
    {% endif %}
          restartPolicy: Never
