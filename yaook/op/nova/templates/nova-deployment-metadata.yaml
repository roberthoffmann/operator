##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "nova-metadata"
  labels:
    app: "nova-metadata"
spec:
  replicas:  {{ crd_spec.metadata.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['config'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "nova-metadata"
          image: {{ versioned_dependencies['nova_docker_image'] }}
          command: ["nova-api-metadata"]
          imagePullPolicy: Always
          volumeMounts:
            - name: nova-config-volume
              mountPath: /etc/nova/nova.conf
              subPath: nova.conf
            - name: ca-certs
              mountPath: /etc/ssl/certs
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-bundle.crt
          livenessProbe:
            exec:
              command:
                - curl
                - localhost:8080
          readinessProbe:
            exec:
              command:
                - curl
                - localhost:8080
          resources: {{ crd_spec | resources('metadata.nova-metadata') }}
        - name: "ssl-terminator"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "8775"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9090"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-bundle.crt
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8775
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8775
              scheme: HTTPS
          resources: {{ crd_spec | resources('metadata.ssl-terminator') }}
        - name: "service-reload"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('metadata.service-reload') }}
      volumes:
        - name: nova-config-volume
          secret:
            secretName: {{ dependencies['config'].resource_name() }}
            items:
              - key: nova.conf
                path: nova.conf
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_nova_metadata_certificate_secret'].resource_name() }}
        - name: ssl-terminator-config
          emptyDir: {}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
