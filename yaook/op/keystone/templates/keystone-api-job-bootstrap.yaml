##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: batch/v1
kind: Job
metadata:
  generateName: "keystone-bootstrap-"
spec:
  template:
    spec:
        automountServiceAccountToken: false
        enableServiceLinks: false
        containers:
          - name: keystone-setup
            image:  {{ versioned_dependencies['keystone_docker_image'] }}
            command: ["sh", "-c"]
            args:
              -
                keystone-manage bootstrap
                  --bootstrap-password="${ADMIN_PASSWORD}"
                  --bootstrap-username="${ADMIN_USERNAME}"
                  --bootstrap-project-name="${ADMIN_PROJECT_NAME}"
                  --bootstrap-admin-url="${KEYSTONE_ADMIN_URL}"
                  --bootstrap-internal-url="${KEYSTONE_INTERNAL_URL}"
                  --bootstrap-public-url="${KEYSTONE_PUBLIC_URL}"
                  --bootstrap-region-id="${REGION_ID}"
            env:
            - name: ADMIN_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: {{ dependencies['admin_credentials'].resource_name() }}
                  key: OS_PASSWORD
            - name: ADMIN_USERNAME
              valueFrom:
                secretKeyRef:
                  name: {{ dependencies['admin_credentials'].resource_name() }}
                  key: OS_USERNAME
            - name: ADMIN_PROJECT_NAME
              valueFrom:
                secretKeyRef:
                  name: {{ dependencies['admin_credentials'].resource_name() }}
                  key: OS_PROJECT_NAME
            - name: KEYSTONE_ADMIN_URL
              value: {{ "https://keystone.%s.svc:5000/v3/" | format(namespace) }}
            - name: KEYSTONE_INTERNAL_URL
{% if crd_spec.api.internal | default(False) %}
              value: {{ "https://%s:%d/v3/" | format(crd_spec.api.internal.ingress.fqdn, crd_spec.api.internal.ingress.port) }}
{% else %}
              value: {{ "https://keystone.%s.svc:5000/v3/" | format(namespace) }}
{% endif %}
            - name: KEYSTONE_PUBLIC_URL
              value: {{ "https://%s:%d/v3/" | format(crd_spec.api.ingress.fqdn, crd_spec.api.ingress.port) }}
            - name: REGION_ID
              value: {{ crd_spec.region.name }}
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
            volumeMounts:
              - name: keystone-config-volume
                mountPath: /etc/keystone/keystone.conf
                subPath: keystone.conf
              - name: keystonecredential
                mountPath: /etc/keystone/credential-keys
              - name: keystonefernet
                mountPath: /etc/keystone/fernet-keys
              - name: ca-certs
                mountPath: /etc/pki/tls/certs
            resources: {{ crd_spec | resources('job.keystone-bootstrap-job') }}
        volumes:
          - name: keystone-config-volume
            secret:
              secretName: {{ dependencies['config'].resource_name() }}
              items:
                - key: keystone.conf
                  path: keystone.conf
          - name: keystonecredential
            secret:
              secretName: {{ dependencies['credential_keys'].resource_name() }}
          - name: keystonefernet
            secret:
              secretName: {{ dependencies['fernet_keys'].resource_name() }}
          - name: ca-certs
            configMap:
              name: {{ dependencies['ca_certs'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
        imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
        restartPolicy: Never
