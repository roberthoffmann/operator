##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "heat-api-cfn"
  labels:
    app: "heat-api-cfn"
spec:
  replicas:  {{ crd_spec.apiCfn.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['config_api_cfn'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "heat-api-cfn"
          image:  {{ versioned_dependencies['heat_docker_image'] }}
          imagePullPolicy: Always
          command: ["heat-api-cfn"]
          volumeMounts:
            # We use subPath instead of projected volume here since we
            # want to prevent pre-existing files to be overwritten.
            - name: heat-config-volume
              mountPath: /etc/heat/heat.conf
              subPath: heat.conf
            - name: heat-config-volume
              mountPath: /etc/heat/policy.json
              subPath: policy.json
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          env:
            - name: LOCAL_PORT
              value: "8080"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
          livenessProbe:
            exec:
              command:
                - curl
                - localhost:8080
          readinessProbe:
            exec:
              command:
                - curl
                - localhost:8080
          resources: {{ crd_spec | resources('apiCfn.heat-api-cfn') }}
        - name: "ssl-terminator"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "8000"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9090"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8000
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8000
              scheme: HTTPS
          resources: {{ crd_spec | resources('apiCfn.ssl-terminator') }}
        - name: "ssl-terminator-external"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "8001"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9091"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8001
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8001
              scheme: HTTPS
          resources: {{ crd_spec | resources('apiCfn.ssl-terminator-external') }}
{% if crd_spec.apiCfn.internal | default(False) %}
        - name: "ssl-terminator-internal"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: Always
          env:
            - name: SERVICE_PORT
              value: "8002"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9092"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8002
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8002
              scheme: HTTPS
          resources: {{ crd_spec | resources('apiCfn.ssl-terminator-internal') }}
{% endif %}
        - name: "service-reload"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-config
              mountPath: /config
            - name: tls-secret
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('apiCfn.service-reload') }}
        - name: "service-reload-external"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('apiCfn.service-reload-external') }}
{% if crd_spec.apiCfn.internal | default(False) %}
        - name: "service-reload-internal"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: Always
          volumeMounts:
            - name: ssl-terminator-internal-config
              mountPath: /config
            - name: tls-secret-internal
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('apiCfn.service-reload-internal') }}
{% endif %}
      volumes:
        - name: heat-config-volume
          projected:
            sources:
            - secret:
                name: {{ dependencies['config_api_cfn'].resource_name() }}
                items:
                  - key: heat.conf
                    path: heat.conf
            - configMap:
                name: {{ dependencies['heat_policy'].resource_name() }}
                items:
                  - key: policy.json
                    path: policy.json
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_certificate_secret_api_cfn'].resource_name() }}
        - name: tls-secret-external
          secret:
            secretName: {{ dependencies['api_cfn_external_certificate_secret'].resource_name() }}
        - name: ssl-terminator-config
          emptyDir: {}
        - name: ssl-terminator-external-config
          emptyDir: {}
{% if crd_spec.apiCfn.internal | default(False) %}
        - name: tls-secret-internal
          secret:
            secretName: {{ dependencies['api_cfn_internal_certificate_secret'].resource_name() }}
        - name: ssl-terminator-internal-config
          emptyDir: {}
{% endif %}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
