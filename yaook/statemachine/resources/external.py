#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.statemachine.extresources` – Objects representing non-Kubernetes resources
#######################################################################################

.. autosummary::

    ExternalResource

.. autoclass:: ExternalResource

"""  # noqa
import abc
import json
import typing

from .. import context
from .base import DependencyMap, Resource


class ExternalResource(Resource):
    """
    Represent a non-Kubernetes object’s state.

    :param finalizer: A Kubernetes finalizer string which is used to allow
        coordinated deletion of the external resource.

    This state makes use of Kubernetes’ finalizer feature in order to prevent
    resource deletion without the cooperation of this state. This allows to
    require that the external resource has been cleaned up properly before the
    Kubernetes resource to which this state belongs gets deleted.

    The finalizer is thus attached to the Kubernetes resource to which this
    state belongs. Based on the lifecycle events of that resource, the methods
    of this state are called.

    **Public interface:**

    .. autoattribute:: component

    .. automethod:: get_listeners

    .. automethod:: is_ready

    .. automethod:: reconcile

    **Documentation for subclasses:**

    In order to implement a :class:`ExternalResource`, the following methods
    must be implemented:

    .. automethod:: update

    .. automethod:: delete

    """

    def __init__(
            self,
            *,
            finalizer: str,
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)
        self.finalizer = finalizer

    @abc.abstractmethod
    async def update(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> None:
        """
        Update the object to conform to the declared state.

        .. seealso::

            :meth:`~.Resource.reconcile`
                for details on `dependencies`.

        :meth:`update` must be idempotent and it must be able to cope with
        having been interrupted at any point, including before its execution.
        That means that it has to be able to deal with all kinds of partially
        or not at all created states.
        """

    @abc.abstractmethod
    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> None:
        """
        Delete the object to which the declared state refers.

        .. seealso::

            :meth:`~.Resource.reconcile`
                for details on `dependencies`.

        :meth:`delete` must be idempotent. It cannot assume that :meth:`update`
        has completed successfully before it is called on an object. It must be
        able to deal with partially created instances, even if :meth:`update`
        provides strong exception safety.
        """

    async def reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap) -> None:
        """
        Reconcile the external resource state.

        :param ctx: The context of the operation.
        :param dependencies: The dependencies of the operation.

        .. seealso::

            :meth:`~.Resource.reconcile`
                for details on `dependencies`.

        This method uses finalizers to implement correct sequencing of calls
        to :meth:`update` and :meth:`delete` to maintain the external object
        instance.
        """
        metadata = ctx.parent.get("metadata", {})
        finalizers = metadata.get("finalizers", [])
        deletion_timestamp = metadata.get("deletionTimestamp", None)
        has_finalizer = self.finalizer in finalizers

        if deletion_timestamp is not None:
            if not has_finalizer:
                # object is not under our watch (anymore), hence we do nothing
                return

            # delete the external resource and *then* remove the finalizer.
            await self.delete(ctx, dependencies)

            # see below for why/how we use SSA here
            await ctx.parent_intf.patch(
                ctx.namespace, ctx.parent_name,
                json.dumps(
                    {
                        "apiVersion": ctx.parent_api_version,
                        "kind": ctx.parent_kind,
                        "metadata": {
                            "name": ctx.parent_name,
                            "finalizers": [],
                        },
                    },
                ).encode("utf-8"),
                field_manager=self.finalizer,
            )
            return

        if not has_finalizer:
            # we use an SSA patch here in order to avoid issues with
            # conflicting patches
            # NOTE: we have to use a unique field manager here in order to
            # avoid accidentally deleting other changes
            # NB: we cannot use the standard strategic merge (which would be
            # perfectly fine for finalizers) because that is not supported with
            # custom resources by the k8s API.
            await ctx.parent_intf.patch(
                ctx.namespace, ctx.parent_name,
                json.dumps(
                    {
                        "apiVersion": ctx.parent_api_version,
                        "kind": ctx.parent_kind,
                        "metadata": {
                            "name": ctx.parent_name,
                            "finalizers": [self.finalizer],
                        },
                    },
                ).encode("utf-8"),
                field_manager=self.finalizer,
            )

        await self.update(ctx, dependencies)

    async def cleanup_orphans(
            self,
            ctx: context.Context,
            protect: typing.Any,
    ) -> None:
        pass
