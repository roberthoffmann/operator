#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""
:mod:`~yaook.statemachine.excceptions` – Exceptions of the statemachine
#######################################################################

Exception types
===============

.. currentmodule:: yaook.statemachine

.. autoclass:: ResourceNotPresent

.. autoclass:: AmbiguousRequest

.. autoclass:: ConfigurationInvalid

.. autoclass:: ContinueableError

"""
import typing

from . import context


class ResourceLookupError(LookupError):
    def __init__(self,
                 component: str,
                 context: context.Context,
                 *,
                 resource_version: typing.Optional[str] = None):
        super().__init__(
            f"failed to find unique resource for component {component!r} "
            f"of {context.parent_kind}.{context.parent_api_version} "
            f"{context.namespace}/{context.parent_name} "
            f"(with instance {context.instance!r})"
        )
        self.resource_version = resource_version


class ResourceNotPresent(ResourceLookupError):
    pass


class AmbiguousRequest(ResourceLookupError):
    pass


class ConfigurationInvalid(Exception):
    """
    Exception being thrown if the configuration of the resource is invalid.

    :param message: The message for the user

    This exception signifies an error in the content of the configuration.
    Retrying will not help.
    """
    def __init__(self, message: str) -> None:
        super().__init__(message)
        self.message = message


class ContinueableError(Exception):
    """
    An exception that indicates that an individual operation failed, but the
    process should nevertheless continue.

    :param msg: The message of the error.
    :param ready: If the operator should regard the throwing state as ready
        for evaluating its dependencies (default is False)

    This error should be raised by a State if the processing of this state
    failed, but the operator should not evaluate this as a failed reconcileing
    task. This Error is therefor used to differenciate between a bug in the
    operator itself (e.g. a KeyError) and an expected error state (e.g. failing
    to generate configuration, because the input is invalid). In the former
    case we can not do anything but abort the processing (as it is done with
    normal errors). In the latter case we can continue with all other states
    of the CR as this issue should not affect them.

    Note that the operator might evaluate dependencies of this state after
    this exception has been raised if ready is True.
    """
    def __init__(self, msg: str, ready: bool = False):
        super().__init__(msg)
        self.msg = msg
        self.ready = ready


class TriggerReconcile(Exception):
    """
    Signal that a direct reconcile is desired. Can be called from inside
    the reconcile.
    """
    def __init__(self, message: str) -> None:
        super().__init__(message)
        self.message = message
