{{- define "yaook.operator.name" -}}
nova
{{- end -}}

{{- define "yaook.operator.extraEnv" -}}
- name: YAOOK_EVICT_MANAGER_IMAGE
  value: {{ include "yaook.operator-lib.image" . }}
{{- end -}}
