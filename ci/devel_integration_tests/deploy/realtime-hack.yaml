##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

# Background:
#
# The OS configuration we use in the CI uses an unfortunate combination of
# advanced realtime scheduling and suboptimal defaults. That causes the
# improved mariadb configuration to not spawn there.
#
# To disable that, we have to set the kernel.sched_rt_runtime_us sysctl to
# -1. We do this as a daemonset tolerating all the things because that is
# easier than reasoning about where we need to do this or fixing the image
# used by the CI clusters.
#
# For full background/info see https://gitlab.com/yaook/operator/-/issues/121#note_548059706
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: realtime-hack
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: realtime-hack
  template:
    metadata:
      labels:
        app.kubernetes.io/name: realtime-hack
    spec:
      containers:
      - name: watchkeeper
        image: debian:bullseye
        command:
        - bash
        - -c
        - |
          set -euo pipefail
          apt-get update
          apt-get install -y --no-install-recommends tini procps
          cat > /main.sh <<EOF
          sysctl kernel.sched_rt_runtime_us
          while true; do
            sysctl kernel.sched_rt_runtime_us=-1
            sleep 120
          done
          EOF
          chmod +x /main.sh
          exec tini -- /main.sh
        env:
        - name: TINI_SUBREAPER
          value: "1"
        - name: TINI_KILL_PROCESS_GROUP
          value: "1"
        securityContext:
          privileged: true
      tolerations:
      - effect: NoSchedule
        operator: Exists
      - effect: NoExecute
        operator: Exists
